#include <vector>

enum fieldState{NOT_CLICKED, FLAGGED, UNCERTAIN, REVEALED, BOMB};

class Game
{
    private:
        unsigned int sizeX;
        unsigned int sizeY;
        unsigned int start_mines;
        int flagged_mine_cnt{};
        std::vector<std::vector<int>> is_bomb{};
        std::vector<std::vector<fieldState>> field_state{};

        bool win_condition();
        int make_move(int, int, char);
        void init_field();
        void display_field();
        int check_neighbours(int, int);

    public:
        bool isValid(int, int);
        bool play_the_game();        
        
        Game(char);
        ~Game();
};


