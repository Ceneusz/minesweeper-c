#include <iostream>
#include <chrono>
#include "game.h"

using namespace std;

void displayMenu(){
    cout << endl << endl << endl;
    cout<<"MINESWEEPER" << endl<<endl;
    cout<<"E - start game (easy)" << endl;
    cout<<"M - start game (medium)" << endl;
    cout<<"H - start game (hard)" << endl;
    cout<<"Q - quit"<<endl<<endl;
}

int main(){

    char c;
    bool game=true;

    do{
        displayMenu();

        cin >> c;
        c = toupper(c);

        switch(c){
            case 'E':
            case 'M':
            case 'H':
                {
                    Game g(c);
                    bool b;

                    auto begin = chrono::system_clock::now();
                    auto end = chrono::system_clock::now();   
                    b = g.play_the_game();
                    
                    if(b){
                        for(size_t i=0; i<10; i++) cout<<'\n';
                        cout<<"U won"<<endl<<endl;
                        end = chrono::system_clock::now();
                        auto diff = end-begin;

                        // cout << "Time: " << diff.count()*chrono::microseconds::<<endl; 
                    }
                    else{
                        cout<<"U lose"<<endl;
                    }

                    break;
                }
            case 'Q':
                game = false;
                break;
            default:
                cout << "Wrong command" << endl;
        }

    } while(game);

    

    return 0;
}