#include "game.h"
#include <vector>
#include <iostream>
#include <math.h>
#include <time.h>
#include <random>
#include <chrono>

using namespace std;

Game::Game(char difficulty)
{
    /*
        Class constructor. It initialises game's parameters, based on
        chosen difficulty level.
        E - easy
        M - medium
        H - hard
    */

    switch(difficulty){
        case 'E':
            sizeX = 8;
            sizeY = 8;
            start_mines = 10;
            break;
        case 'M':
            sizeX = 16;
            sizeY = 16;
            start_mines = 40;
            break;
        case 'H':
            sizeX = 30;
            sizeY = 16;
            start_mines = 99;
            break;
    }
}

Game::~Game(){
    /*
        Constructor remains empty, since there
        is no junk memory to get rid off.
    */
}

bool Game::isValid(int x, int y){
    /*
    Simple function for checking, if cell's
    coordinates don't go out of field's
    bounds.
     */
    return x>=0 && x<sizeX && y>=0 && y<sizeY;
}

void Game::init_field(){

    // initialisation of table of states
    vector<fieldState> vec_tmp(sizeX,NOT_CLICKED);
    vector<vector<fieldState>> vec_tmp2(sizeY,vec_tmp);

    field_state = vec_tmp2;


    // initialisation of table which shows whether
    // there on given field there is hidden mine
    vector<int> vec_tmp3(sizeX,0);
    vector<vector<int>> vec_tmp4(sizeY,vec_tmp3);

    is_bomb = vec_tmp4;

    int i=0;

    // setting up random generators
    unsigned seed = chrono::steady_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);

    normal_distribution<double> distributionX(ceil(sizeX/2),ceil(sizeX/4));
    normal_distribution<double> distributionY(ceil(sizeY/2),ceil(sizeY/4));

    // generating i number of unique mine locations
    while(i<this->start_mines){
        int x = distributionX(generator);
        int y = distributionY(generator);
        x = x > sizeX-1 ? sizeX-1 : x;
        x = x < 0 ? 0 : x;
        y = y > sizeY-1 ? sizeY-1 : y;
        y = y < 0 ? 0 : y;

        if(is_bomb[y][x]!=1){
            is_bomb[y][x] = 1;
            i++;
        }
    }

}

void Game::display_field(){
    /*
        Function, which displays current board state,
        based on current states of each field.
    */
    cout<<endl<<endl<<endl<<endl;
    cout<<"Flagged fields: "<<this->flagged_mine_cnt<<endl<<endl;
    
    string line(2*sizeX+3, '-');
    cout<<line<<endl;

    for(int y=0; y<sizeY; y++){
        cout<< '|';
        for(int x=0; x<sizeX; x++){
            fieldState s = this->field_state[y][x];
            switch (s)
            {
                case NOT_CLICKED:
                    cout<<" #";
                    break;
                case FLAGGED:
                    cout<<" %";
                    break;
                case UNCERTAIN:
                    cout<<" ?";
                    break;
                case REVEALED:
                    {
                        int count = this->check_neighbours(x, y);
                        cout<<' '<<count;
                    }
                    break;
                case BOMB:
                    cout<<" X";
                    break;
                default:
                    break;
            }
        }

        cout << " |" << endl;
    }

    cout<<line<<endl;
}

int Game::make_move(int x, int y, char c){
    c = toupper(c);
    bool bomb_touched = false;

    switch (c)
    {
        case 'C':
            if(this->field_state[y][x]==NOT_CLICKED){
                if(this->is_bomb[y][x]){
                    bomb_touched = true;
                    this->field_state[y][x] = BOMB;
                }
                else{
                    this->field_state[y][x]=REVEALED;
                    if(this->check_neighbours(x,y)==0){
                        for(int i=x-1;i<x+2;i++){
                            for(int j=y-1;j<y+2;j++){
                                if(isValid(i,j)){
                                    if(this->field_state[j][i]==NOT_CLICKED) this->make_move(i,j,'C');
                                }
                            }
                        }
                    }
                }
            }
            break;
        case 'F':
            if(this->field_state[y][x]==NOT_CLICKED){
                this->field_state[y][x] = FLAGGED;
                this->flagged_mine_cnt++;
            }
            else if(this->field_state[y][x]==FLAGGED){
                this->field_state[y][x] = NOT_CLICKED;
                this->flagged_mine_cnt--;
            }
            break;
        case 'U':
            if(this->field_state[y][x]==NOT_CLICKED) this->field_state[y][x] = UNCERTAIN;
            if(this->field_state[y][x]==UNCERTAIN) this->field_state[y][x] = NOT_CLICKED;
            break;
        default:
            cout<<"Wrong command"<<endl;
            break;
    }

    return bomb_touched;
}

int Game::check_neighbours(int x, int y){
    /*
        Function which checks how many mines
        are around chosen cell.
    */
    
    int count = 0;

    for(int i = y-1; i<y+2; i++){
        for(int j = x-1; j<x+2; j++){
            if(isValid(j, i)){
                count += this->is_bomb[i][j];
            }
        }
    }

    return count;
}

bool Game::play_the_game(){
    /*
        Event loop, which allows to actually
        play the game. It calls functions responsible
        for game logic, including making a move and
        checking win and lose condition.
     */
    char in;
    int x,y;
    bool bomb_found;
    
    this->init_field();

    while (true)
    {
        this->display_field();
        cout<<"Select move: "<<endl;
        cout<<"C - click\nF - flag/unflag\nU - mark as uncertain"<<endl;
        cin >> in;

        do
        {
            cout<<"Select cell: "<<endl;
            cin >> x >> y;
        } while (!isValid(x,y));
        
        bomb_found = this->make_move(x,y,in);

        if(bomb_found){
            for (size_t i = 0; i < sizeX; i++){
                for (size_t j = 0; j < sizeY; j++){
                    if(this->is_bomb[j][i]) this->field_state[j][i]=BOMB;
                }
            }

            this->display_field();
            
            return false;
        }
        else if(this->win_condition()){
            return true;
        }

    }
            
}

bool Game::win_condition(){
    /* 
        Function which checks if there are
        any not clicked fields which don't contain
        mine. If there aren't, win condition is fulfiled. 
    */

    for(size_t i=0; i<sizeY; i++){
        for(size_t j=0; j<sizeY; j++){
            if(this->field_state[i][j]==NOT_CLICKED
            && this->is_bomb[i][j]==0){
                return false;
            }
        }
    }

    return true;
}
